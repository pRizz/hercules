package server

import (
	"strings"
	"testing"

	"../config"
	"../logs"
	"../utils/network"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	expectedConnectionType = "udp"
	expectedIdentifier     = "77.55.235.204"
	expectedHostname       = "field.deviota.com"
	expectedPort           = "443"
	invalidConnectionType  = "tcp"

	addresses = []string{
		expectedIdentifier + ":" + expectedPort,
		expectedIdentifier,

		expectedConnectionType + "://" + expectedIdentifier,
		expectedConnectionType + "://" + expectedIdentifier + ":" + expectedPort,

		invalidConnectionType + "://" + expectedIdentifier + ":" + expectedPort,
	}
)

func TestGetConnectionTypeAndIdentifierAndPort(t *testing.T) {
	restartConfig()

	for _, address := range addresses {
		logs.Log.Info("Running test with neighbor's address: " + address)
		basicNodeInfo, err := getBasicNodeInfo(address)

		if basicNodeInfo.ConnectionType != expectedConnectionType && basicNodeInfo.ConnectionType != invalidConnectionType || err != nil || basicNodeInfo.Identifier != expectedIdentifier {
			t.Error("Not all URI parameters have been detected!")
		} else {

			if strings.Contains(address, expectedPort) {
				if basicNodeInfo.Port != expectedPort {
					t.Error("An invalid port was returned!")
				}
			} else {
				if basicNodeInfo.Port != config.AppConfig.GetString("node.port") {
					t.Error("An invalid port was returned!")
				}
			}
		}
	}
}

func TestAddNeighbor(t *testing.T) {

	restartConfig()

	Neighbors = make(map[string]*Neighbor)

	for _, address := range addresses {
		logs.Log.Info("Running test with neighbor's address: " + address)
		err := AddNeighbor(address)

		if strings.HasPrefix(address, invalidConnectionType) {
			if err == nil {
				t.Error("Added invalid neighbor!")
			}
		} else {
			if err != nil {
				t.Error("Could not add neighbor!")
			}

			for _, neighbor := range Neighbors {
				addr, _ := getConnectionType(address)
				if strings.Contains(address, expectedPort) {
					if neighbor.Addr != addr {
						t.Errorf("Add neighbor %v does not match with loaded %v", neighbor.Addr, addr)
					}
				} else {
					configPort := config.AppConfig.GetString("node.port")
					addressWithConfigPort := addr + ":" + configPort
					if neighbor.Addr != addressWithConfigPort {
						t.Errorf("Add neighbor %v does not match with loaded %v", neighbor.Addr, addressWithConfigPort)
					}
				}

			}

			err = RemoveNeighbor(address)

			if err != nil {
				t.Error("Error during test clean up")
			}
		}

		if len(Neighbors) > 0 {
			logs.Log.Fatal("Test clean up did not work as intended")
		}
	}

}

func TestCheckNeighbourExistsByIPAddressWithPort(t *testing.T) {
	restartConfig()

	Neighbors = make(map[string]*Neighbor)

	testAddresses := []string{
		expectedIdentifier + ":" + expectedPort,
		expectedHostname + ":" + expectedPort,
	}

	for _, address := range testAddresses {
		logs.Log.Info("Running test with neighbor's address: " + address)

		err := AddNeighbor(address)
		if err != nil {
			t.Error("Error during test set up")
		}

		basicNodeInfo, err := getBasicNodeInfo(address)
		if err != nil {
			t.Error("Error during test set up")
		}

		ips, _, err := getIPsAndHostname(basicNodeInfo.Identifier)
		if err != nil {
			t.Error("Error during test set up")
		}

		ip := ips[0].ToString()
		ipWithPort := networkutils.GetFormattedAddress(ip, basicNodeInfo.Port)

		neighborsExists, neighbor := CheckNeighbourExistsByIPAddressWithPort(ipWithPort)
		if !neighborsExists {
			t.Error("Neighbor does NOT exist!")
		} else {
			formattedAddress := networkutils.GetFormattedAddress(basicNodeInfo.Identifier, basicNodeInfo.Port)
			if neighbor.Addr != formattedAddress || neighbor.IP != ip || neighbor.Port != basicNodeInfo.Port {
				t.Error("Neighbor was found but it is NOT the same as the one which was searched for!")
			}
		}

		// Clean-up
		err = RemoveNeighbor(address)
		if err != nil {
			t.Error("Error during test clean up")
		}

		if len(Neighbors) > 0 {
			logs.Log.Fatal("Test clean up did not work as intended")
		}
	}
}

func TestRemoveNeighbor(t *testing.T) {

	restartConfig()

	Neighbors = make(map[string]*Neighbor)

	for _, address := range addresses {
		logs.Log.Info("Running test with neighbor's address: " + address)
		err := AddNeighbor(address)

		if !strings.HasPrefix(address, invalidConnectionType) && err != nil {
			t.Error("Error during test set up")
		}

		err = RemoveNeighbor(address)

		if strings.HasPrefix(address, invalidConnectionType) {
			if err == nil {
				t.Error("Removed invalid neighbor!")
			}
		} else {
			if err != nil {
				t.Error("Could not remove neighbor!")
			}
		}

		if len(Neighbors) > 0 {
			logs.Log.Fatal("Test did not work as intended")
		}
	}

}

func TestGetIpAndHostname(t *testing.T) {

	ips, hostname, err := getIPsAndHostname(expectedHostname)
	ip := ips[0].ToString()

	if err != nil || ip == "" || hostname == "" {
		t.Error("Could not get IP and Hostname for " + expectedHostname)
	}
}

func TestGetURIAndAPIPort(t *testing.T) {
	emptyAddress := ""
	nodeAddress, APIProtocol, APIPort := getURIAndAPIInfo(emptyAddress)
	if nodeAddress != "" || APIProtocol != "" || APIPort != "" {
		t.Errorf("Empty result was expected but something else was returned")
	}

	addressWithoutExtension := "udp://my.node.com:14600"
	nodeAddress, APIProtocol, APIPort = getURIAndAPIInfo(addressWithoutExtension)
	if nodeAddress != addressWithoutExtension || APIPort != "" {
		t.Error("Could not get URI when input address doesn't have an API Port")
	}

	expectedAPIPort := "14265"
	expectedAPIProtocol := "http"
	addressWithExtension := addressWithoutExtension + "?apiport=" + expectedAPIPort + "&apiprotocol=" + expectedAPIProtocol
	nodeAddress, APIProtocol, APIPort = getURIAndAPIInfo(addressWithExtension)
	if nodeAddress != addressWithoutExtension || APIProtocol != expectedAPIProtocol || APIPort != expectedAPIPort {
		t.Error("Could not get URI when input address has an Extension")
	}

	addressWithExtension = addressWithoutExtension + "/?apiport=" + expectedAPIPort + "&apiprotocol=" + expectedAPIProtocol
	nodeAddress, APIProtocol, APIPort = getURIAndAPIInfo(addressWithExtension)
	if nodeAddress != addressWithoutExtension || APIProtocol != expectedAPIProtocol || APIPort != expectedAPIPort {
		t.Error("Could not get URI when input address has an Extension")
	}
}

func TestUpdateLastSnapshotTime(t *testing.T) {
	neighbor := &Neighbor{
		APIInfo: &APIInfo{
			APIEndPoint: "http://hercules.iota.love:14265",
		},
	}
	neighbor.UpdateLastSnapshotTime()
}

func restartConfig() {
	config.AppConfig = viper.New()
	flag.IntP("node.port", "u", 14600, "UDP Node port")
	flag.Parse()
	config.AppConfig.BindPFlags(flag.CommandLine)
}
