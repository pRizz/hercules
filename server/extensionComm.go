package server

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

type InterNodeRequest struct {
	Command string
}

type GetNodeInfoResponse struct {
	AppName    string `json:"appName"`
	AppVersion string `json:"appVersion"`
}

type SnapshotInfo struct {
	Timestamp         int64  `json:"timestamp"`
	TimeHumanReadable string `json:"TimeHumanReadable"`
	Path              string `json:"path"`
	Checksum          string `json:"checksum"`
}

type GetLatestSnapshotInfoResponse struct {
	CurrentSnapshotTimestamp            int64        `json:"currentSnapshotTimestamp"`
	CurrentSnapshotTimeHumanReadable    string       `json:"currentSnapshotTimeHumanReadable"`
	IsSynchronized                      bool         `json:"isSynchronized"`
	UnfinishedSnapshotTimestamp         int64        `json:"unfinishedSnapshotTimestamp"`
	UnfinishedSnapshotTimeHumanReadable string       `json:"unfinishedSnapshotTimeHumanReadable"`
	InProgress                          bool         `json:"inProgress"`
	LatestSnapshot                      SnapshotInfo `json:"latestSnapshot"`
	Time                                int64        `json:"time"`
	Duration                            int32        `json:"duration"`
}

func sendRequest(request interface{}, url string) (responseBytes []byte, err error) {
	req, err := generateRequest(request, url)
	if err != nil {
		return nil, err
	}

	client := getClient(req)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	responseBodyBytes, _ := ioutil.ReadAll(resp.Body)
	return responseBodyBytes, nil
}

func getClient(request *http.Request) *http.Client {
	client := &http.Client{Timeout: time.Second * 5}
	return client
}

func generateRequest(request interface{}, url string) (*http.Request, error) {
	requestBytes, _ := json.Marshal(request)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBytes))
	req.Header.Set("X-IOTA-API-VERSION", "1")
	req.Header.Set("Content-Type", "application/json")

	return req, err
}
