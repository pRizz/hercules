package tangle

import (
	"bytes"
	"sync/atomic"
	"time"

	"../convert"
	"../crypt"
	"../db"
	"../db/coding"
	"../db/ns"
	"../logs"
	"../server"
	"../snapshot"
	"../transaction"
	"../utils"
)

const (
	// These are the probabilities from IRI
	P_TIP_REPLY              = 66
	P_BROADCAST              = 10
	P_SEND_MILESTONE         = 2
	P_PROPAGATE_REQUEST      = 1
	P_SELECT_MILESTONE_CHILD = 70
	P_REMOVE_REQUEST         = 1
)

func incomingRunner() {
	srv.IncomingWaitGroup.Add(1)
	defer srv.IncomingWaitGroup.Done()

	for {
		select {
		case <-srv.IncomingQueueQuit:
			return

		case rawMsg := <-srv.Incoming:
			server.TrackIncoming(1)

			processedAsBlocked, senderNeighbor := processAsBlocked(rawMsg)
			if processedAsBlocked {
				// Skip message
				continue
			}

			senderNeighbor.TrackIncoming(1)
			processIncomingRawMsg(rawMsg, senderNeighbor)
		}
	}
}

func processAsBlocked(rawMsg *server.RawMsg) (processedAsBlocked bool, neighborFound *server.Neighbor) {
	ipAddressWithPort := (*rawMsg.Addr).String() // Format <ip>:<port>
	neighborExists, neighbor := server.CheckNeighbourExistsByIPAddressWithPort(ipAddressWithPort)
	if neighborExists {
		neighborFound = neighbor
	} else {
		server.TrackBlocked(1)
		processedAsBlocked = true
	}
	return
}

// The raw message contains a TX received from the neighbor and the hash of a TX they want (or empty if they want a tip)
func processIncomingRawMsg(rawMsg *server.RawMsg, neighbor *server.Neighbor) {
	incomingTXHashBytes := processRequest(rawMsg, neighbor)
	requestedTxHashBytes, neighborRequestedTip := getRequestedTXHashBytesFromRawData(rawMsg, incomingTXHashBytes)
	sendMessageToNeighbor(neighbor, nil, requestedTxHashBytes, neighborRequestedTip)
}

//
// Process incoming data
//
func processRequest(rawMsg *server.RawMsg, neighbor *server.Neighbor) (incomingTXHashBytes []byte) {
	hashOfTXProcessedAsUnsuccessful, processedAsUnsucessful, incomingTXFound := processAsUnsuccessfulOnPrecheck(rawMsg, neighbor)
	if processedAsUnsucessful {
		return hashOfTXProcessedAsUnsuccessful
	}

	incomingTX := incomingTXFound
	setTXAsTemporarilyKnown(incomingTX)
	server.TrackNewTx(1)
	processIncomingTX(rawMsg, incomingTX, neighbor)
	return
}

func processAsUnsuccessfulOnPrecheck(rawMsg *server.RawMsg, neighbor *server.Neighbor) (hashOfTXProcessedAsUnsuccessful []byte, processedAsUnsucessful bool, incomingTXFound *transaction.FastTX) {
	incomingTXBytes := (*rawMsg.Data)[:DATA_SIZE]
	tipTXHash, processedAsTip := processAsTipTX(incomingTXBytes)
	if processedAsTip {
		return tipTXHash, true, nil
	}

	knownTXHash, processedAsKnown := processAsKnownTX(incomingTXBytes)
	if processedAsKnown {
		return knownTXHash, true, nil
	}

	incomingTX := transaction.BytesToTX(incomingTXBytes)

	hashOfTXWithUnacceptableTimestamp, processedAsTxWithUnacceptedTimestamp := processAsTxWithUnacceptedTimestamp(incomingTX, neighbor)
	if processedAsTxWithUnacceptedTimestamp {
		return hashOfTXWithUnacceptableTimestamp, true, incomingTX
	}

	hashOfInvalidTx, processedAsInvalid := processAsInvalid(incomingTX, neighbor)
	if processedAsInvalid {
		return hashOfInvalidTx, true, incomingTX
	}

	// Check again because another thread could have received
	// the same TX from another neighbor in the meantime
	knownTXHash, processedAsKnown = processAsKnownTX(incomingTXBytes)
	if processedAsKnown {
		return knownTXHash, true, nil
	}

	return nil, false, incomingTX
}

func processAsInvalid(tx *transaction.FastTX, neighbor *server.Neighbor) (hashOfInvalidTx []byte, processedAsInvalid bool) {
	if !crypt.IsValidPoW(tx.Hash, MWM) {
		neighbor.TrackInvalid(1)
		hashOfInvalidTx = tx.Hash
		processedAsInvalid = true
	}
	return
}

func processAsTipTX(txBytes []byte) (tipTXHashFound []byte, processedAsTip bool) {
	isTXTip := transaction.IsTip(txBytes)
	if isTXTip {
		server.TrackTipReq(1)
		tipTXHashFound = tipHashKey
		processedAsTip = true
	}
	return
}

func processAsKnownTX(txBytes []byte) (txHashFound []byte, processedAsKnown bool) {
	txHashFoundByFingerprint, err := fingerPrintCache.Get(txBytes)
	txFoundInCache := err == nil // TX was not received in the last <fingerPrintTTL> seconds
	if txFoundInCache {
		server.TrackKnownTx(1)
		txHashFound = txHashFoundByFingerprint
		processedAsKnown = true
	}
	return
}

func processAsTxWithUnacceptedTimestamp(tx *transaction.FastTX, neighbor *server.Neighbor) (hashOfTXWithUnacceptableTimestamp []byte, processedAsTxWithUnacceptedTimestamp bool) {
	isTxTimeAcceptable := checkIfTimeIsAcceptable(int64(tx.Timestamp))
	if !isTxTimeAcceptable {
		server.TrackPreSnap(1)
		neighbor.TrackPreSnapOrFuture(1)
		hashOfTXWithUnacceptableTimestamp = tx.Hash
		processedAsTxWithUnacceptedTimestamp = true
	}
	return
}

func setTXAsTemporarilyKnown(tx *transaction.FastTX) {
	fingerPrintCache.Set(tx.Bytes, tx.Hash, fingerPrintTTL)
}

func getRequestedTXHashBytesFromRawData(rawMsg *server.RawMsg, incomingTXHashBytes []byte) (requestedTXHashBytes []byte, neighborRequestedTip bool) {
	rawData := *rawMsg.Data
	requestedTXHashBytes = make([]byte, HASH_SIZE)
	copy(requestedTXHashBytes, rawData[DATA_SIZE:PACKET_SIZE])

	neighborRequestedTip = bytes.Equal(requestedTXHashBytes, transaction.TipBytes[:HASH_SIZE]) || (bytes.Equal(requestedTXHashBytes, incomingTXHashBytes))
	if neighborRequestedTip {
		server.TrackRandomTipReq(1)
	}
	return requestedTXHashBytes, neighborRequestedTip
}

func checkIfTimeIsAcceptable(txTimestamp int64) bool {
	snapTime := snapshot.GetSnapshotTimestamp(nil)
	futureTime := time.Now().Add(2 * time.Hour).Unix()

	isTxTimeAcceptable := snapTime < txTimestamp && txTimestamp < futureTime
	return isTxTimeAcceptable
}

func processIncomingTX(rawMsg *server.RawMsg, tx *transaction.FastTX, neighbor *server.Neighbor) {
	var pendingMilestone *PendingMilestone

	// Hash the TxHash to a DB Hashkey
	key := ns.HashKey(tx.Hash, ns.NamespaceHash)

	err := db.Singleton.Update(func(dbTx db.Transaction) (e error) {
		// TODO: catch error defer here

		removePendingRequest(tx.Hash)

		if dbTx.HasKey(ns.Key(key, ns.NamespaceSnapshotted)) {
			// Tx was snapshotted

			requestIfMissing(tx.TrunkTransaction, false, dbTx)
			requestIfMissing(tx.BranchTransaction, false, dbTx)

			dbTx.Remove(ns.Key(key, ns.NamespacePendingConfirmed))
			dbTx.Remove(ns.Key(key, ns.NamespaceEventConfirmationPending))

			snapTime := snapshot.GetSnapshotTimestamp(dbTx)

			err := coding.PutInt64(dbTx, ns.Key(key, ns.NamespaceEdge), snapTime)

			_checkIncomingError(tx, err)
			/*
				parentKey, err := dbTx.GetBytes(ns.Key(key, ns.NamespaceEventMilestonePairPending))
				if err == nil {
					err = dbTx.Remove(ns.Key(parentKey, ns.NamespaceEventMilestonePending))
					_checkIncomingError(tx, err)
				}
				dbTx.Remove(ns.Key(key, ns.NamespaceEventMilestonePairPending))
			*/
			return nil
		}

		if !dbTx.HasKey(key) {
			// Tx is not in the database yet

			err := SaveTX(tx, &tx.Bytes, dbTx)
			_checkIncomingError(tx, err)

			isMilestone := isMaybeMilestone(tx)
			if isMilestone {
				// Tx seems to be the first part of a milestone and was not in the database before => add it to the pending queue
				trunkBytesKey := ns.HashKey(tx.TrunkTransaction, ns.NamespaceBytes)
				pendingMilestone = &PendingMilestone{Key: key, TX2BytesKey: trunkBytesKey, LastCheck: time.Now().Add(-milestoneCheckInterval)}
				// Mark the milestone as pending in the database, trunkBytesKey is the Tx Hash of the missing second part of the milestone
				err := dbTx.PutBytes(ns.Key(key, ns.NamespaceEventMilestonePending), trunkBytesKey)
				_checkIncomingError(tx, err)
			}

			// Each trunk/branch of every milestone or every Tx that is marked in the DB as solidification are put in a high priority queue for faster sync
			// TODO: Should be done by milestone by milestone from older to latest, to get faster solid milestones
			isSolidity := isMilestone || dbTx.HasKey(ns.Key(key, ns.NamespacePendingSolidification))
			requestIfMissing(tx.TrunkTransaction, isSolidity, dbTx)
			requestIfMissing(tx.BranchTransaction, isSolidity, dbTx)

			// EVENTS:

			pendingConfirmationKey := ns.Key(key, ns.NamespacePendingConfirmed)
			if dbTx.HasKey(pendingConfirmationKey) {
				err = dbTx.Remove(pendingConfirmationKey)
				_checkIncomingError(tx, err)

				err = addPendingConfirmation(key, int64(tx.Timestamp), dbTx)
				_checkIncomingError(tx, err)
			}

			// Re-broadcast new TX. Not always.
			// Here, it is actually possible to favor nearer neighbours!
			if utils.Random(0, 100) < P_BROADCAST {
				Broadcast(tx.Bytes, neighbor)
			}

			neighbor.TrackNew(1)
			saved++
			atomic.AddInt64(&totalTransactions, 1)
		} else {
			discarded++
		}
		return nil
	})

	if err == nil {
		if pendingMilestone != nil {
			addPendingMilestoneToQueue(pendingMilestone)
		}
	} else {
		addPendingRequest(tx.Hash, true, 0, false, nil)

		atomic.AddInt64(&totalTransactions, -1)
		neighbor.TrackNew(-1)
	}

	if err == nil {
		server.TrackValid(1)
		neighbor.LastIncomingTime = time.Now()
	} else {
		processIncomingTXError(rawMsg, tx, err)
	}
}

func processIncomingTXError(rawMsg *server.RawMsg, tx *transaction.FastTX, err error) {
	if err == db.ErrTransactionConflict {
		fingerPrintCache.Del(tx.Bytes)
		select {
		case srv.Incoming <- rawMsg: // Process the message again
		default: // Drop the message if Incoming queue is full to prevent deadlocks
		}
		return
	}
	if err != nil {
		logs.Log.Errorf("Processing incoming message failed! Err: %v", err)
		return
	}
}

func _checkIncomingError(tx *transaction.FastTX, err error) {
	if err != nil {
		logs.Log.Errorf("Failed processing TX %v", convert.BytesToTrytes(tx.Hash)[:81], err)
		panic(err)
	}
}
