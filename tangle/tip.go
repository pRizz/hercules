package tangle

import (
	"sync"
	"time"

	"../db"
	"../db/coding"
	"../db/ns"
	"../logs"
	"../server"
	"../transaction"

	"github.com/muxxer/freecache"
)

var (
	tipsCacheSize = 1 * 1024 * 1024                   // 1MB
	TipsCache     = freecache.NewCache(tipsCacheSize) // Key = Hash, Value = time.Time ReceiveTimestamp

	tipRemoverTicker     *time.Ticker
	tipRemoverWaitGroup  = &sync.WaitGroup{}
	tipRemoverTickerQuit = make(chan struct{})
)

func tipOnLoad() {
	loadTipsFromDB()
	go tipsRemover()
}

func loadTipsFromDB() {
	var tipsToRemove [][]byte
	var tipsCnt uint64
	var tipsLoadedCnt uint64

	db.Singleton.View(func(dbTx db.Transaction) error {
		return coding.ForPrefixInt64(dbTx, ns.Prefix(ns.NamespaceTip), true, func(key []byte, timestamp int64) (bool, error) {
			tipsCnt++

			hash, err := dbTx.GetBytes(ns.Key(key, ns.NamespaceHash))
			if err != nil {
				// Tip doesn't exist
				tipsToRemove = append(tipsToRemove, key)
				return true, nil
			}

			tipAge := time.Since(time.Unix(timestamp, 0))

			if tipAge >= maxTipAge {
				// Tip is too old
				tipsToRemove = append(tipsToRemove, key)
				return true, nil
			}

			tipTTL := int((maxTipAge - tipAge).Nanoseconds() / int64(time.Second))

			tipsLoadedCnt++
			TipsCache.Set(hash, make([]byte, 0), tipTTL)

			return true, nil
		})
	})
	logs.Log.Infof("Loaded tips: %v\n", tipsLoadedCnt)

	logs.Log.Infof("Tips to remove: %v/%v", len(tipsToRemove), tipsCnt)

	// Remove invalid tips in the database
	db.Singleton.Update(func(dbTx db.Transaction) error {
		for _, key := range tipsToRemove {
			dbTx.Remove(key)
		}
		return nil
	})
}

func tipsRemover() {
	tipRemoverWaitGroup.Add(1)
	defer tipRemoverWaitGroup.Done()

	executeTipsRemover()

	tipRemoverTicker = time.NewTicker(tipRemoverInterval)
	for {
		select {
		case <-tipRemoverTickerQuit:
			return

		case <-tipRemoverTicker.C:
			if ended {
				break
			}
			executeTipsRemover()
		}
	}
}

func executeTipsRemover() {
	tipsToRemove, tipsCnt := getTipsToRemove()
	logs.Log.Infof("Tips to remove: %v/%v", len(tipsToRemove), tipsCnt)
	removeTips(tipsToRemove)
}

func getTipsToRemove() (tipsToRemove [][]byte, tipsCnt int) {

	iter := TipsCache.NewIterator()

	db.Singleton.View(func(dbTx db.Transaction) error {
		for {
			tipsCnt++

			entry := iter.Next()
			if entry == nil {
				// If there is no more entries to return, nil will be returned.
				break
			}

			if dbTx.CountPrefix(ns.HashKey([]byte(entry.Key), ns.NamespaceApprovee)) > 0 {
				// Tip was already approved
				tipsToRemove = append(tipsToRemove, entry.Key)
			}
		}
		return nil
	})

	return tipsToRemove, tipsCnt
}

func addTip(hash []byte, timestamp int64, dbTx db.Transaction) error {

	tipAge := time.Since(time.Unix(timestamp, 0))

	if tipAge >= maxTipAge {
		// Tip is too old
		return nil
	}

	if dbTx.CountPrefix(ns.HashKey(hash, ns.NamespaceApprovee)) > 0 {
		// Tip was already approved
		return nil
	}

	tipTTL := int((maxTipAge - tipAge).Nanoseconds() / int64(time.Second))
	TipsCache.Set(hash, make([]byte, 0), tipTTL)

	return coding.PutInt64(dbTx, ns.Key(hash, ns.NamespaceTip), timestamp) // Mark Tx as tip in the DB for next hercules startup
}

func removeTips(tipsToRemove [][]byte) {
	// Tips are only removed in the cache, not in the DB anymore
	// This is due to the fact, that the Tips in the DB are only checked at hercules startup
	for _, hash := range tipsToRemove {
		TipsCache.Del(hash)
	}
}

func updateTipsOnNewTransaction(tx *transaction.FastTX, dbTx db.Transaction) (err error) {
	err = addTip(tx.Hash, int64(tx.Timestamp), dbTx)

	tipsToRemove := [][]byte{tx.TrunkTransaction, tx.BranchTransaction}
	removeTips(tipsToRemove)

	return err
}

func getRandomTip(dbTx db.Transaction, neighbor *server.Neighbor) (hash []byte, txBytes []byte) {

	if dbTx == nil {
		dbTx = db.Singleton.NewTransaction(false)
		defer dbTx.Discard()
	}

	// TODO: If Solidity is implemented in hercules, at first solid tips should be returned (see IRI)
	for retry := 0; retry < 10; retry++ {
		key, _, err := TipsCache.GetRandomValue()
		if err != nil {
			continue
		}

		hash = key

		if !isTxTimestampAcceptable(hash, neighbor, dbTx) {
			continue
		}

		txBytes, err := dbTx.GetBytes(ns.HashKey(hash, ns.NamespaceBytes))
		if err != nil {
			return nil, nil
		}

		return hash, txBytes
	}
	return nil, nil
}

func isTxTimestampAcceptable(txHashBytes []byte, neighbor *server.Neighbor, dbTx db.Transaction) bool {
	if neighbor == nil || neighbor.APIInfo.LastSnapshotTimeStamp == 0 {
		return true // No neighbor or Extension is not configured
	}

	if dbTx == nil {
		dbTx = db.Singleton.NewTransaction(false)
		defer dbTx.Discard()
	}

	txTimestamp, err := GetTXTimestamp(txHashBytes, dbTx)
	if err != nil {
		return false
	}

	timestampIsAcceptable := txTimestamp > neighbor.APIInfo.LastSnapshotTimeStamp
	return timestampIsAcceptable
}
