package crypt

import (
	"testing"
)

var (
	// Trytes used for hashTest
	// "RWOMFTUFECPAAOUJCXZRUKTWNLSIMDPZLGLUT9XKJKECAIJBYPTTDUYDNRHMJNKKFJMPHEQR9TNFZ9999"
	hashTestBytes = []byte{139, 45, 58, 74, 192, 29, 16, 3, 198, 91, 166, 175, 48, 155, 138, 39, 107, 40, 218, 117, 110, 48, 255, 98, 64, 16, 10, 36, 88, 156, 47, 11, 236, 152, 147, 120, 158, 207, 58, 118, 143, 48, 246, 174, 137, 235, 0, 0, 0}
)

func TestIsValidPoW(t *testing.T) {
	isValidPoW := IsValidPoW(hashTestBytes, 14)

	if !isValidPoW {
		t.Error("Invalid PoW")
	}

}
